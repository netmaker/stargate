package main

import (
	"fmt"
        "github.com/gravitl/netmaker/netclient/functions"
        "golang.zx2c4.com/wireguard/wgctrl"
        nodepb "github.com/gravitl/netmaker/grpc"
	"flag"
	"os"
        "os/exec"
        "strconv"
	"strings"
	"log"
)

const (
	// name of the service
	name        = "netclient"
	description = "Netmaker Daemon Service"
)

var password string
var network string
var server string
var accesskey string

var (
        wgclient *wgctrl.Client
)

var (
        wcclient nodepb.NodeServiceClient
)

func main() {
	tpassword := flag.String("p", "changeme", "This node's password for accessing the server regularly")
	taccesskey := flag.String("k", "badkey", "an access key generated by the server and used for one-time access (install only)")
	taccesstoken := flag.String("t", "badtoken", "an token generated by the server and used for one-time access (install only)")
	tname := flag.String("name", "noname", "give the node a name at runtime")
	tserver := flag.String("s", "localhost:50051", "The location (including port) of the remote gRPC server.")
	tnetwork := flag.String("n", "nonetwork", "The node network you are attempting to join.")
	tdnsoff := flag.Bool("dnsoff", false, "DNS Mode. If true, netclient will not alter system dns. false by default.")
	tpublicip := flag.String("ip4", "nopubip", "The node network you are attempting to join.")
	tnoauto := flag.Bool("na", false, "No auto mode. If true, netmclient will not be installed as a system service and you will have to retrieve updates manually via checkin command.")
	tipforward := flag.String("nf", "on", "No Forward mode. If true, netclient will not check for IP forwarding. This may break functionality")
	command := flag.String("c", "required", "The command to run")


        flag.Parse()



         getID := exec.Command("id", "-u")
         out, err := getID.Output()

         if err != nil {
                 log.Fatal(err)
         }
         id, err := strconv.Atoi(string(out[:len(out)-1]))

         if err != nil {
                 log.Fatal(err)
         }

         if id != 0 {
                 log.Fatal("This program must be run with elevated privileges (sudo). This program installs a SystemD service and configures WireGuard and networking rules. Please re-run with sudo/root.")
         }


	_, err = exec.LookPath("wg")
	if err != nil {
		log.Println(err)
		log.Fatal("WireGuard not installed. Please install WireGuard (wireguard-tools) and try again.")
	}

        switch *command {
		case "getport":
			portno, err := functions.GetFreePort(51821)
			fmt.Printf("Port Number: %v", portno)
			fmt.Println("")
			if err != nil {
				log.Fatal(err)
			}
		case "required":
                        fmt.Println("command flag 'c' is required. Pick one of |install|checkin|update|remove|")
                        os.Exit(1)
			log.Fatal("Exiting")
                case "install":

                        if *taccesstoken == "badtoken" && (*tnetwork == "nonetwork"  || *tnetwork == "") {
                                fmt.Println("Required, '-n'. No network provided. Exiting.")
                                os.Exit(1)
                        }
			/*
			if !*tnoforward {
				forward := exec.Command("sysctl", "net.ipv4.ip_forward")
				out, err := forward.Output()

				if err != nil {
					log.Fatal(err)
				}
				//s := strings.Split(string(out), " ", "\n")
				s := strings.Fields(string(out))
				if err != nil {
					log.Fatal(err)
				}
				if s[2] != "1" {
					log.Fatal("It is recommended to enable IP Forwarding. Current status is: " +  s[2] + ", but should be 1. if you would like to run without IP Forwarding, re-run with flag '-nf true'")
				}
			}
			*/
			fmt.Println("Beginning agent installation.")
			err := functions.Install(*taccesskey, *tpassword, *tserver, *tnetwork, *tnoauto, *taccesstoken, *tname, *tpublicip, *tdnsoff, *tipforward)
			if err != nil {
				fmt.Println("Error encountered while installing.")
				if !strings.Contains(err.Error(), "ALREADY_INSTALLED") {
				fmt.Println("Error installing: ", err)
				fmt.Println("Cleaning up (uninstall)")
				err = functions.Remove(*tnetwork)
				if err != nil {
                                        fmt.Println("Error uninstalling: ", err)
                                        fmt.Println("Wiping local.")
					err = functions.WipeLocal(*tnetwork)
					if err != nil {
						fmt.Println("Error removing artifacts: ", err)
					}
                                        err = functions.RemoveSystemDServices(*tnetwork)
                                        if err != nil {
                                                fmt.Println("Error removing services: ", err)
                                        }
				}
				os.Exit(1)
				} else {
					fmt.Println(err.Error())
					os.Exit(1)
				}
			}
		/*
		case "service-install":
                        fmt.Println("Beginning service installation.")
                        err := functions.ConfigureSystemD()
                        if err != nil {
                                fmt.Println("Error installing service: ", err)
                                os.Exit(1)
                        }
                case "service-uninstall":
                        fmt.Println("Beginning service uninstall.")
                        err := functions.RemoveSystemDServices()
                        if err != nil {
                                fmt.Println("Error installing service: ", err)
                                os.Exit(1)
                        }
		*/
		case "checkin":
                        if *tnetwork == "nonetwork" || *tnetwork == "" {
                                fmt.Println("Required, '-n'. No network provided. Exiting.")
                                os.Exit(1)
                        }
			fmt.Println("Beginning node check in for network " + *tnetwork)
			err := functions.CheckIn(*tnetwork)
			if err != nil {
				fmt.Println("Error checking in: ", err)
				os.Exit(1)
			}
		case "remove":
			if *tnetwork == "nonetwork" || *tnetwork == "" {
                                fmt.Println("Required, '-n'. No network provided. Exiting.")
                                os.Exit(1)
			}
                        fmt.Println("Beginning node cleanup.")
			err := functions.Remove(*tnetwork)
                        if err != nil {
					/*
                                        fmt.Println("Error uninstalling: ", err)
                                        fmt.Println("Wiping local.")
                                        err = functions.WipeLocal()
                                        if err != nil {
                                                fmt.Println("Error removing artifacts: ", err)
                                        }
                                        err = functions.RemoveSystemDServices()
                                        if err != nil {
                                                fmt.Println("Error removing services: ", err)
                                        }
					*/
                                fmt.Println("Error deleting node: ", err)
                                os.Exit(1)
                        }
		default:
			fmt.Println("You must select from the following commands: install|remove|checkin", err)
			os.Exit(1)

	}
	fmt.Println("Command " + *command + " Executed Successfully")
}
